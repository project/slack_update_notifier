<?php

/**
 * @file
 * Slack Update Notifier module admin functions.
 */

/**
 * Admin form.
 */
function slack_update_notifier_configure_form($form, &$form_state) {
  $form['slack_update_notifier_sitename'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#description' => t('This will be used as the title for the message posted to Slack'),
    '#default_value' => variable_get('slack_update_notifier_sitename'),
    '#required' => TRUE,
  );

  $form['slack_update_notifier_webhook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Webhook URL'),
    '#description' => t('Enter your Webhook URL from an Incoming WebHooks integration. It looks like https://hooks.slack.com/services/XXXXXXXXX/YYYYYYYYY/ZZZZZZZZZZZZZZZZZZZZZZZZ'),
    '#default_value' => variable_get('slack_update_notifier_webhook_url'),
    '#required' => TRUE,
  );

  $form['slack_update_notifier_channel'] = array(
    '#type' => 'textfield',
    '#title' => t('Channel'),
    '#description' => t('Enter your channel name with # symbol, for example #general (or @username for a private message or a private group name).'),
    '#default_value' => variable_get('slack_update_notifier_channel'),
    '#required' => TRUE,
  );

  $form['slack_update_notifier_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('What would you like to name your Slack bot?'),
    '#default_value' => variable_get('slack_update_notifier_username'),
    '#required' => TRUE,
  );

  $form['slack_update_notifier_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t('The message to send to Slack. A link to the updates page will automatically be included.'),
    '#default_value' => variable_get('slack_update_notifier_message'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
