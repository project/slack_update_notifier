Slack Update Norifier for Drupal 7

Slack Update Notifier allows you to send custom messages to Slack
whenever an update is available for Drupal Core.

Installation instructions:

1.	Login to your Slack account and setup an Incoming WebHook
2.	Download and enable the Slack Update Notifier Module
3.	Go to /admin/config/services/slack-update-notifier/config and
fill in all the settings
